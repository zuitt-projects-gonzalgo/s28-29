//First we load the expressjs module into our application and saved it in a variable called express
const express = require('express');

//create an application with expressjs
	//this creates an application that uses express and stores it as app
	//app is our server
const app = express();

//port is a variable that contains the port number we want to designate to our server
const port = 4000;

//middleware
//express.json() is a method from express which allows us to handle the streaming of data and automatically parse the incoming JSON from our req body
//app.use is used to run a method or another function for our expressjs api
app.use(express.json());

let users = [
	{
		userName : "BMadridal",
		email : "fateReader@mail.com",
		password : "weDontTalkAboutMe"
	},
	{
		userName : "LuisaMadridal",
		email : "strongSis@mail.com",
		password : "pressure"
	}
];

let items = [
	{
		name : "roses",
		price : 170,
		isActive : true
	},
	{
		name : "tulips",
		price : 250,
		isActive : false
	}
];

//Express has a methods to use as routes corresponding to HTTP methods
//app.get(<endpoint>, <function handling req and res>)
app.get('/', (req, res) => {

	//once the route is accessed, we can send a response with the use of res.send()
		//res.send() actually combines writeHead() and end()
	res.send('Hello From ExpressJS Api!')
});

app.get('/greeting', (req, res) => {
	res.send('Hello from Batch169-gonzalgo')
})

app.get('/users', (req, res) => {
	res.send(users);
})

//how do we get data from the client as a request body?
app.post('/users', (req, res) => {
	console.log(req.body);
	let newUser = {
		userName : req.body.userName,
		email: req.body.email,
		password: req.body.password
	}
	users.push(newUser);
	console.log(users)
	res.send(users);
})

//deleting users route
app.delete('/users', (req, res) => {
	users.pop();
	res.send(users);
})

//update users route
app.put('/users/:index', (req, res) => {
	//req.body - this will contain the updated password
	console.log(req.body);
	//req.params object which contains the value in the url params
	//url params is captured by route parameter (:parameterName) and saved as property in req.params
	console.log(req.params);

	let index = parseInt(req.params.index);
	//get the user that we want to update with our index number from url params
	users[index].password = req.body.password

	//send the updated user to the client
	//provide the index variabe to be the index for the particular item in the array
	res.send(users[index]);
})

app.get('/users/getSingleUser/:index', (req, res) => {
	let index = parseInt(req.params.index);
	res.send(users[index]);
})

//mini-activity
app.get('/items', (req, res) => {
	res.send(items);
})

app.post('/items', (req, res) => {
	let newItem = {
		name : req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}
	items.push(newItem);
	res.send(items);
})

app.put('/items/updateSingleItemPrice/:index', (req, res) => {
	let index = parseInt(req.params.index);
	items[index].price = req.body.price
	res.send(items[index]);
})



// A - C - T - I - V - I - T - Y (Activity)

/*
>> Create a new route with '/items/getSingleItem/:index' endpoint. 
	>> This route should allow us to GET the details of a single item.
		-Pass the index number of the item you want to get via the url as url params in your postman.
		-http://localhost:4000/items/getSingleItem/<indexNumberOfItem>
	>> Send the request and back to your api/express:
		-get the data from the url params.
		-send the particular item from our array using its index to the client.

>> Create a new route to update an item with '/items/archive/:index' endpoint.	
	>> This route should allow us to UPDATE the isActive property of our product.
		-Pass the index number of the item you want to de-activate via the url as url params.
		-Access the particular item with its index. Access the isActive property of the item and re-assign it to false.
		-send the updated item in the client

>> Create a new route to update an item with '/items/activate/:index' endpoint. 
	>> This route should allow us to UPDATE the isActive property of our product.
		-Pass the index number of the item you want to activate via the url as url params.
		-Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
		-send the updated item in the client

>> Save the Postman collection in your s28-s29 folder

*/
app.get('/items/getSingleItem/:index', (req, res) => {
	let index = parseInt(req.params.index);
	res.send(items[index]);
})

app.put('/items/archive/:index', (req, res) => {
	let index = parseInt(req.params.index);
	items[index].isActive = req.body.isActive;
	res.send(items[index]);
})

app.put('/items/activate/:index', (req, res) => {
	let index = parseInt(req.params.index);
	items[index].isActive = req.body.isActive;
	res.send(items[index]);
})




//listen method, server listens to the assigned port
app.listen(port, () => console.log(`Server is running at port ${port}`))